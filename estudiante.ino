
#include <WiFi.h>
#include <SPI.h>
#include <PubSubClient.h>  // biliboteca MQTT
#include <Adafruit_GFX.h>
#include <Max72xxPanel.h>
const int Boton_1 = 25;
const int Boton_2 = 26;
const int Boton_3 = 27;
const int Boton_4 = 33;
const int Boton_5 = 32;
const int Led_red = 12;
const int Led_green = 13;

//**************Botonera*************
int estado_1 = 0; //Estado de oprimido del boton
int estado_2 = 0;
int estado_3 = 0;
int estado_4 = 0;
int estado_5 = 0;
int estadoanterior_1 = 0;
int estadoanterior_2 = 0;
int estadoanterior_3 = 0;
int estadoanterior_4 = 0;
int estadoanterior_5 = 0;
int espera = 250;

//*****************WIFI****************
//Conexion a WiFI
char* ssid = "Familia Pinzon Gomez"; //Nombre de la red 
char* wifi_password = "JESUS2603910"; // Contraseña del wifi

//***************MQTT**************
const char* mqtt_server = "192.168.1.18"; //Direccion IP del Broker (Gestor de datos), Mosquitto instalado en la Raspberry, osea direccion IP de la Raspberry
const char* mqtt_topic = "botones"; //Canal de comunicacion llamado Respuesta
const char* mqtt_topic_2 = "Peticion"; //Canal de comunicacion llamado Peticion
const char* mqtt_username = "Inalde"; //Pasword de acceso para los topic 1 y 2 
const char* mqtt_password = "Sabana";
char* clientID = "10"; //ID asignado al ESP


//************Configuracion Pantalla***************
int pinCS = 15; //Puerto del ESP
int matrix_horizontal = 4; //Cantidad de pantallas conectadas en el horizaontal
int matrix_vertical = 1; //Cantidad de pantallas conectadas en el vertical
Max72xxPanel matrix = Max72xxPanel(pinCS, matrix_horizontal, matrix_vertical); //Se instancia el objeto "matrix" de la clase Max72xxPanel de la libreria de la matriz.
int espacio = 1; //Espacio entre letras en la visualizacion
int width = 5 + espacio;
int x=0;
int y=0;
int i=0;

// Conexion WiFI
WiFiClient wifiClient;
PubSubClient client(mqtt_server, 1883, wifiClient);




void setup() {
  matrix.setIntensity(3);
  matrix.setRotation(0, 1);
  matrix.setRotation(1, 1);
  matrix.setRotation(2, 1);
  matrix.setRotation(3, 1);
  matrix.fillScreen(LOW);
  pinMode(Boton_1, INPUT);
  pinMode(Boton_2, INPUT);
  pinMode(Boton_3, INPUT);
  pinMode(Boton_4, INPUT);
  pinMode(Boton_5, INPUT);
  pinMode(Led_red, OUTPUT);
  pinMode(Led_green, OUTPUT);
  Serial.begin(115200);
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, wifi_password);
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(Led_red, HIGH);
    //digitalWrite(Led_green, HIGH);
    delay(250);
    digitalWrite(Led_red, LOW);
    delay(250);
    Serial.print(".");
  }
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  if (Connect()) {
    Serial.println("Connected to MQTT Broker!");
    digitalWrite(Led_red, LOW);
  }
  else {
    Serial.println("Connection to MQTT Broker failed...");
    digitalWrite(Led_red, HIGH);
    digitalWrite(Led_green, LOW);

  }
  client.setCallback(ReceivedMessage);
  delay(10);
  client.publish(mqtt_topic, clientID);
  delay(espera);
}
void loop() {
String cadena= clientID;
estado_1=digitalRead(Boton_1);
while(estado_1==HIGH){
  Serial.println("oprimido");
estadoanterior_1=HIGH;
estado_1=digitalRead(Boton_1);
}
if (estado_1!=estadoanterior_1) {
    cadena+="A";
    int tam=cadena.length()+1;
    char nuevoArray[tam];
    cadena.toCharArray(nuevoArray, tam);
    client.publish(mqtt_topic, nuevoArray);
    digitalWrite(Led_green, HIGH);
    Serial.println(cadena);
    delay(espera);
    digitalWrite(Led_green, LOW);
  }
estadoanterior_1=digitalRead(Boton_1);
estado_2=digitalRead(Boton_2);
while(estado_2==HIGH){
  Serial.println("oprimido");
estadoanterior_2=HIGH;
estado_2=digitalRead(Boton_2);
}
if (estado_2!=estadoanterior_2) {
    cadena+="B";
    int tam=cadena.length()+1;
    char nuevoArray[tam];
    cadena.toCharArray(nuevoArray, tam);
    client.publish(mqtt_topic, nuevoArray);
    digitalWrite(Led_green, HIGH);
    Serial.println(cadena);
    delay(espera);
    digitalWrite(Led_green, LOW);
  }
estadoanterior_2=digitalRead(Boton_2);
delay(espera);
estado_3=digitalRead(Boton_3);
while(estado_3==HIGH){
  Serial.println("oprimido");
estadoanterior_3=HIGH;
estado_3=digitalRead(Boton_3);
}
if (estado_3!=estadoanterior_3) {
    cadena+="C";
    int tam=cadena.length()+1;
    char nuevoArray[tam];
    cadena.toCharArray(nuevoArray, tam);
    client.publish(mqtt_topic, nuevoArray);
    digitalWrite(Led_green, HIGH);
    Serial.println(cadena);
    delay(espera);
    digitalWrite(Led_green, LOW);
  }
estadoanterior_3=digitalRead(Boton_3);
estado_4=digitalRead(Boton_4);
while(estado_4==HIGH){
  Serial.println("oprimido");
estadoanterior_4=HIGH;
estado_4=digitalRead(Boton_4);
}
if (estado_4!=estadoanterior_4) {
    cadena+="D";
    int tam=cadena.length()+1;
    char nuevoArray[tam];
    cadena.toCharArray(nuevoArray, tam);
    client.publish(mqtt_topic, nuevoArray);
    digitalWrite(Led_green, HIGH);
    Serial.println(cadena);
    delay(espera);
    digitalWrite(Led_green, LOW);
  }
estadoanterior_4=digitalRead(Boton_4);
estado_5=digitalRead(Boton_5);
while(estado_5==HIGH){
  Serial.println("oprimido");
estadoanterior_5=HIGH;
estado_5=digitalRead(Boton_5);
}
if (estado_5!=estadoanterior_5) {
    cadena+="P";
    int tam=cadena.length()+1;
    char nuevoArray[tam];
    cadena.toCharArray(nuevoArray, tam);
    client.publish("botones", nuevoArray);
    digitalWrite(Led_green, HIGH);
    Serial.println(cadena);
    delay(espera);
    digitalWrite(Led_green, LOW);
  }
estadoanterior_5=digitalRead(Boton_5);

  if (!client.connected()) {
    Connect();
  }
  client.loop();

}
void ReceivedMessage(char* topic, byte* payload, unsigned int length) {
  char nombre[length];
  for (int i = 0; i < length; i++) {
    nombre[i]=(char)(payload[i]);
  }
Serial.println("");
String str= nombre;
String cliente= clientID;
String idCliente= cliente+".";
int max = str.length() - idCliente.length();
bool cont=Contains(str,idCliente);
if(cont){
  for (int i = idCliente.length(); i <str.length() ; i++) {
      Serial.print(nombre[i]);
      matrix.drawChar(x, y, nombre[i], HIGH, LOW, 1);
      x=x+6;
  } 
  matrix.write();
}
}

char* Pasar(String cadena, int tam){

  char nuevoArray[tam];
  cadena.toCharArray(nuevoArray, tam);
  for (int o = 0; o < tam; o++) {
    Serial.print(nuevoArray[o]);
  }
  Serial.println("");
  return nuevoArray;
}
bool Contains( String s, String search)
{
  int tamMax=s.length();
  int tamMin=search.length();
  int max = tamMax - tamMin; // the searchstring has to fit in the other one
  for (int i=0; i<= max; i++)
  {
  if (s.substring(i,tamMin) == search) {
    return true;
  }  // or i
  }
return false;  //or -1
}

bool Connect() {
  // Connect to MQTT Server and subscribe to the topic
  if (client.connect(clientID, mqtt_username, mqtt_password)) {
      client.subscribe(mqtt_topic_2);
      return true;
    }
    else {
      return false;
  }

}